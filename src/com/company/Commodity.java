package com.company;

public class Commodity {
    private  int id;
    private  int productCode;
    private double price;
    private  String name;
    private  String description;

    public Commodity(int id, int productCode, double price, String name, String description) {
        this.id = id;
        this.productCode = productCode;
        this.price = price;
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return  "id=" + id + "\n" +
                "productCode=" + productCode + "\n" +
                "price=" + price + "\n" +
                "name='" + name  + "\n" +
                "description='" + description;
    }
}

package com.company;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        ArrayList <Commodity> commodityArrayList = new ArrayList<>();

        commodityArrayList.add(new FragileProduct(1, 01, 212.1, "Чашка", "Хрупкая и красивая. Стоит чтобы купить.", 20));

        commodityArrayList.add(new PerishableProduc(2, 02, 280.1, "Песочный замак", "Красивый и скоро развалится. Стоит чтобы купить.", 20));

        commodityArrayList.add(new DimensionalGoods(3, 03, 666.666, "Статуя ильеча", "Принесет много денег. Стоит чтобы купить.", 20, 30, 40));

        for(Commodity commod: commodityArrayList){
            System.out.println(commod.toString());
        }
    }


}

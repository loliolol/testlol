package com.company;

public class PerishableProduc extends Commodity{
    private int maxStorageTime;

    public PerishableProduc(int id, int productCode, double price, String name, String description, int maxStorageTime) {
        super(id, productCode, price, name, description);
        this.maxStorageTime = maxStorageTime;

    }

    @Override
    public String toString() {
        return
                 super.toString() + "\n" +
                 "maxStorageTime=" + maxStorageTime + "\n" ;
    }
}

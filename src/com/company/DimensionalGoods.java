package com.company;

import java.util.ArrayList;

public class DimensionalGoods extends Commodity{
    private  int height;
    private  int width;
    private  int length;
    public DimensionalGoods(int id, int productCode, double price, String name, String description, int height, int width, int length) {
        super(id, productCode, price, name, description);
        this.height = height;
        this.width = width;
        this.length = length;
    }
    @Override
    public String toString() {
        return    super.toString() + "\n" +
                 "height=" + height + "\n" +
                 "width=" + width + "\n" +
                 "length=" + length + "\n" ;
    }
}

package com.company;

public class FragileProduct extends Commodity{
    private int fragilityCoefficient;

    public FragileProduct(int id, int productCode, double price, String name, String description, int fragilityCoefficient) {
        super(id, productCode, price, name, description);
        this.fragilityCoefficient = fragilityCoefficient;
    }

    @Override
    public String toString() {
        return
                 super.toString() + "\n" +
                 "fragilityCoefficient=" + fragilityCoefficient + "\n";
    }
}

